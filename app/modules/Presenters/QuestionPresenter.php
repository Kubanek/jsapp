<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;

final class QuestionPresenter extends Nette\Application\UI\Presenter
{

    private Nette\Database\Explorer $database;

    public function __construct(Nette\Database\Explorer $database)
    {
        parent::__construct();
        $this->database = $database;
    }

    public function actionDefault($categoryId = null, $questionNumber = null)
    {
        if (is_null($categoryId) || !is_int((int)$categoryId)) {
            $this->presenter->sendJson(["Invalid or missing argument CategoryId"]);
        } elseif (is_null($questionNumber) || !is_int((int)$questionNumber)) {
            $this->presenter->sendJson(["Invalid or missing argument QuestionNumber"]);
        }
    }

    public function handleDownloadCategoryQuestions(): void
    {
        $post = $this->presenter->getRequest()->getPost();

        if ($post["id"] === null || !is_int((int)$post["id"])) {
            $this->presenter->sendJson("error");
        }

        $questions = $this->database->table('Question')->where("category_id", (int)$post["id"])->order('id ASC');
        $array = [];

        $questionNumber = 0;
        foreach ($questions as $question) {
            ++$questionNumber;
            $array[$questionNumber] = ["question_info" => $question["question_info"]];
        }

        if (count($array) <= 0) {
            $this->presenter->sendJson("error");
        }

        $this->presenter->sendJson($array);
    }

}
